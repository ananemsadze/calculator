package com.example.calculator

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var FirstNumber = 0.0
    private var SecondNumber = 0.0
    private var Operation = " "




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init() }

    private fun init() {
        Button0.setOnClickListener(this)
        Button1.setOnClickListener(this)
        Button2.setOnClickListener(this)
        Button3.setOnClickListener(this)
        Button4.setOnClickListener(this)
        Button5.setOnClickListener(this)
        Button6.setOnClickListener(this)
        Button7.setOnClickListener(this)
        Button8.setOnClickListener(this)
        Button9.setOnClickListener(this)
        DotButton.setOnClickListener(this)
        DeleteButton.setOnLongClickListener() {
            ResultTV.text = ""
            Operation.isEmpty()
            true }

        DotButton.setOnClickListener {
            if(ResultTV.text.isNotEmpty() && ("." !in ResultTV.text.toString()))
                ResultTV.text = ResultTV.text.toString() + "." }
    }





    fun delete(view: View) {

        val value = ResultTV.text.toString()
        if (value.isNotEmpty())
            ResultTV.text = value.substring(0, value.length - 1)
    }


    fun divide(view: View){
        val value = ResultTV.text.toString()
        if(value.isNotEmpty()){
            FirstNumber = value.toDouble()
            Operation = ":"
            ResultTV.text = ""} }

    fun multiply(view: View){
        val value = ResultTV.text.toString()
        if(value.isNotEmpty()){
            FirstNumber = value.toDouble()
            Operation = "*"
            ResultTV.text = ""}
    }


    fun minus(view: View){
        val value = ResultTV.text.toString()
        if(value.isNotEmpty()){
            FirstNumber = value.toDouble()
            Operation = "-"
            ResultTV.text = ""}
    }

    fun plus(view: View){
        val value = ResultTV.text.toString()
        if(value.isNotEmpty()){
            FirstNumber = value.toDouble()
            Operation = "+"
            ResultTV.text = ""}
    }

    
    fun equals(view: View) {
        val value = ResultTV.text.toString()
        if (value.isNotEmpty() && Operation != "") {
            SecondNumber = value.toDouble()
            var result: Double = 0.0
            if (Operation == ":" && SecondNumber.toDouble() != 0.0) {
                result = FirstNumber / SecondNumber
            }else if (Operation ==":" && SecondNumber.toDouble()==0.0){
                Toast.makeText(this, "Cannot Divide By Zero", Toast.LENGTH_SHORT).show()
            }else if (Operation == "*") {
                result = FirstNumber * SecondNumber
            } else if (Operation == "+") {
                result = FirstNumber + SecondNumber
            } else if (Operation == "-") {
                result = FirstNumber - SecondNumber
            }

            if (result % 1 == 0.0) {
                ResultTV.text = result.toInt().toString()
                if(result.toString().length>=12){
                    ResultTV.text=result.toString()
                }
            } else {
                ResultTV.text = result.toString()
            }


        }
    }


    override fun onClick(v: View?) {
        val button = v as Button
        ResultTV.text = ResultTV.text.toString() + button.text.toString() }


    }



